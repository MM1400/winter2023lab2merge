// marin melentii 2234078
package application;
import vehicles.Bicycle;

public class BikeStore {
    public static void main( String[] args) {
       vehicles.Bicycle[] Bicycles = new vehicles.Bicycle[4];
       Bicycles[0] = new vehicles.Bicycle("fort", 10, 2);
       Bicycles[1] = new vehicles.Bicycle("fort2", 1023, 10);
       Bicycles[2] = new vehicles.Bicycle("fort3", 101, 3);
       Bicycles[3] = new vehicles.Bicycle("fort4", 102, 6);

       for(Bicycle b: Bicycles) {
        System.out.println(b);
       }
    }
}
